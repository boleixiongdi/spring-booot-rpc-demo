package com.demo.demospringcloudfeigen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringcloudFeigenApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringcloudFeigenApplication.class, args);
    }

}

