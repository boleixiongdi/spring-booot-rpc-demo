package com.demo.feignconsumer;

import demo.springcloud.feign.client.AccountQuery;
import demo.springcloud.feign.vo.AccountRequest;
import demo.springcloud.feign.vo.AccountResponse;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

/** 
* FeignConsumerApplication Tester. 
* 
* @author <Authors name> 
* @since <pre>Jan 11, 2019</pre> 
* @version 1.0 
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeignConsumerApplicationTest {

    @Autowired
    AccountQuery accountQuery;

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: main(String[] args) 
* 
*/ 
@Test
public void testMain() throws Exception {

    AccountRequest accountRequest = new AccountRequest();
    accountRequest.setUid("123");
    AccountResponse accountResponse = accountQuery.queryAccount(accountRequest);
    Assert.assertEquals("123", accountResponse.getUid());

} 


} 
