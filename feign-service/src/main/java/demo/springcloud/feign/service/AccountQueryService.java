package demo.springcloud.feign.service;

import demo.springcloud.feign.client.AccountQuery;
import demo.springcloud.feign.vo.AccountRequest;
import demo.springcloud.feign.vo.AccountResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created on 2019/1/11
 * Title: Simple
 * Description: 类描述
 * Copyright: Copyright(c) 2019
 * Company:
 *
 * @author yifeng
 */
@RestController
@RequestMapping("/api/account")
public class AccountQueryService implements AccountQuery {

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    @Override
    public AccountResponse queryAccount(@RequestBody AccountRequest accountRequest) {
        AccountResponse accountResponse = new AccountResponse();
        accountResponse.setUid(accountRequest.getUid());
        accountResponse.setSex(1);
        return accountResponse;
    }

}
