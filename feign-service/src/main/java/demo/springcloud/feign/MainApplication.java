package demo.springcloud.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created on 2019/1/11
 * Title: Simple
 * Description: 类描述
 * Copyright: Copyright(c) 2019
 * Company:
 *
 * @author yifeng
 */

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

}
