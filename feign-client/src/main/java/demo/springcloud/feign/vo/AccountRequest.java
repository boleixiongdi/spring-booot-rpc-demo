package demo.springcloud.feign.vo;

import java.io.Serializable;

/**
 * Created on 2019/1/11
 * Title: Simple
 * Description: 类描述
 * Copyright: Copyright(c) 2019
 * Company:
 *
 * @author yifeng
 */
public class AccountRequest implements Serializable {

    private static final long serialVersionUID = -2225216343569538925L;

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
