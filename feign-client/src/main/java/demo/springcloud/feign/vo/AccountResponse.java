package demo.springcloud.feign.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created on 2019/1/11
 * Title: Simple
 * Description: 类描述
 * Copyright: Copyright(c) 2019
 * Company:
 *
 * @author yifeng
 */
public class AccountResponse implements Serializable {

    private static final long serialVersionUID = -6945630469802028939L;

    private String uid;

    private Integer sex;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

}
