package demo.springcloud.feign.client;

import demo.springcloud.feign.vo.AccountRequest;
import demo.springcloud.feign.vo.AccountResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created on 2019/1/11
 * Title: Simple
 * Description: 类描述
 * Copyright: Copyright(c) 2019
 * Company:
 *
 * @author yifeng
 */
@FeignClient("demo-feign-service")
public interface AccountQuery {

    @RequestMapping(value = "/api/account/query", method = RequestMethod.POST)
    AccountResponse queryAccount(AccountRequest accountRequest);

}
